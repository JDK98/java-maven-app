def buildJar() {
    echo "building the application..."
} 

def buildPushImage() {
    echo "building the docker image..."
} 

def deployApp() {
    echo 'deploying the application...'
} 

return this
